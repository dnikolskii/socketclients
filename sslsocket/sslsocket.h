#ifndef SSLSOCKETNDN202006_H
#define SSLSOCKETNDN202006_H

#ifdef __linux__
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#elif _WIN32
#pragma warning(disable:4996)
#pragma comment(lib, "Ws2_32.lib")
#include <WinSock2.h>
#endif // __linux__


#include <openssl/ssl.h>
#include <openssl/err.h>
#include <cstring>
#include <string>
#include <iostream>
#include <sstream>


enum class SslSocketStatus : int {
    SSL_INIT_STATE = 0,
    SSL_CONNECTED = 1,
    SSL_CREATED = 2,
    SSL_RECEIVIED = 3,
    SSL_SENT = 4,
    SSL_FAILED = 5
};


class SslSocket {
public:
    explicit SslSocket(const char *, int, int debugLevel = 3, int rcvTimeout = 30, int sndTimeout = 30);
    void RecvBuffer(char *buffer, int length);
    void SendBuffer(const char *buffer, int length);
    SslSocketStatus getStatus() {return mStatus;}
    std::string getErrorMessage() {return mErrorMessage;}
    void setDebugLevel(int level) {mDebugLevel = level;}
    ~SslSocket();
private:
    int mDebugLevel {3};
    int mRcvTimeout {30}; // sec.
    int mSndTimeout {30}; // sec.
    SSL *mSsl {nullptr};
    int mSock {0};
    SslSocketStatus mStatus {SslSocketStatus::SSL_INIT_STATE};
    std::string mErrorMessage {""};
};


std::ostream& operator<< (std::ostream &outStream, const SslSocketStatus & sslSocketStatus);

#endif //SSLSOCKETNDN202006_H
