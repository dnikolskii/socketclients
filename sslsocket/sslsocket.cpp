#include "sslsocket.h"


void SslSocket::RecvBuffer(char *buffer, int length)
{
    int len = SSL_read(mSsl, buffer, length);
    if (mDebugLevel >= 3) {
        std::cout << "[SslSocket][Info] Read len = " << len << "; length = " << length << std::endl;
    }
    if (len != length) {
        int err = SSL_get_error(mSsl, len);
        mErrorMessage = std::string(ERR_error_string(err, 0));
        mStatus = SslSocketStatus::SSL_FAILED;
        if (mDebugLevel >= 1) {
            std::cout << "[SslSocket::RecvBuffer][Error] when read nErrorMessage = " << mErrorMessage << std::endl;
        }
        return;
    }

    mStatus = SslSocketStatus::SSL_RECEIVIED;

    if (mDebugLevel >= 2) {
        std::cout << "[SslSocket::RecvBuffer] [debud] " << mStatus << std::endl;
    }
}


void SslSocket::SendBuffer(const char *buffer, int length)
{
    int len = SSL_write(mSsl, buffer, length);
    if (mDebugLevel >= 3) {
        std::cout << "[SslSocket::SendBuffer][Info] write len = " << len << " length = " << length << std::endl;
    }
    if (len != length) {
        int err = SSL_get_error(mSsl, len);
        mErrorMessage = std::string(ERR_error_string(err, 0));
        mStatus = SslSocketStatus::SSL_FAILED;
        if (mDebugLevel >= 1) {
            std::cout << "[SslSocket::SendBuffer][Error] nErrorMessage = " << mErrorMessage << std::endl;
        }
        return;
    }

    mStatus = SslSocketStatus::SSL_SENT;

    if (mDebugLevel >= 2) {
        std::cout << "[SslSocket::SendBuffer] [debud] " << mStatus << std::endl;
    }
}


SslSocket::SslSocket(const char* ip, int port, int debugLevel, int rcvTimeout, int sndTimeout)
{
    mDebugLevel = debugLevel;
    mRcvTimeout = rcvTimeout;
    mSndTimeout = sndTimeout;
    mStatus = SslSocketStatus::SSL_INIT_STATE;

    if (mDebugLevel >= 3) {
        std::cout << "[SslSocket::SslSocket][debug] mDebugLevel = " << mDebugLevel << std::endl;
        std::cout << "[SslSocket::SslSocket][debug] mRcvTimeout = " << mRcvTimeout << std::endl;
        std::cout << "[SslSocket::SslSocket][debug] mSndTimeout = " << mSndTimeout << std::endl;
        std::cout << "[SslSocket::SslSocket][debug] ip = " << ip << std::endl;
        std::cout << "[SslSocket::SslSocket][debug] port = " << port << std::endl;
    }
    bool isSocketCreateError = false;
#ifdef __linux__
    int s = socket(AF_INET, SOCK_STREAM, 0);
    if (s < 0) {
        isSocketCreateError = true;
    }
#elif _WIN32
    int iResult;
    WSADATA wsaData = {0};
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    SOCKET s = socket(AF_INET, SOCK_STREAM, 0);
    if (s == INVALID_SOCKET) {
        isSocketCreateError = true;
    }
#endif // __linux__

    if (isSocketCreateError) {
        mStatus = SslSocketStatus::SSL_FAILED;
        mErrorMessage = "Socket creation failed: error code ";
#ifdef __linux__
        std::stringstream ss;
        ss << s;
        mErrorMessage += ss.str();
#elif _WIN32
        std::stringstream ss;
        ss << WSAGetLastError();
        mErrorMessage += ss.str();
#endif // __linux__
        if(mDebugLevel >= 1) {
            std::cout << "[SslSocket::SslSocket][Error] Socket create Error: " << mErrorMessage << std::endl;
        }
        return;
    }

#ifdef __linux__
    struct timeval tv_rcv;
    tv_rcv.tv_sec = mRcvTimeout;
    tv_rcv.tv_usec = 0;
    setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv_rcv, sizeof tv_rcv);

    struct timeval tv_snd;
    tv_snd.tv_sec = mSndTimeout;
    tv_snd.tv_usec = 0;
    setsockopt(s, SOL_SOCKET, SO_SNDTIMEO, (const char*)&tv_snd, sizeof tv_snd);
#elif _WIN32
    DWORD rcv_timeout = mRcvTimeout * 1000;
    setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (const char*)&rcv_timeout, sizeof rcv_timeout);

    DWORD snd_timeout = mSndTimeout * 1000;
    setsockopt(s, SOL_SOCKET, SO_SNDTIMEO, (const char*)&snd_timeout, sizeof snd_timeout);
#endif // __linux__

    struct sockaddr_in sa;
    memset (&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_addr.s_addr = inet_addr(ip);
    sa.sin_port = htons (port);

    if (connect(s, (struct sockaddr*)&sa, sizeof(sa))) {
        mStatus = SslSocketStatus::SSL_FAILED;
        mErrorMessage = "Socket connection failed";
        if(mDebugLevel >= 1) {
            std::cout << "[SslSocket::SslSocket][Error] Socket connection error: " << mErrorMessage << std::endl;
        }
        return ;
    }

    mStatus = SslSocketStatus::SSL_CREATED;

    //int userTimeout = 10000;  // user timeout in milliseconds [ms]
    //setsockopt (s, SOL_TCP, TCP_USER_TIMEOUT, (char*) &userTimeout, sizeof (userTimeout));

    SSL_library_init();
    SSLeay_add_ssl_algorithms();
    SSL_load_error_strings();
    const SSL_METHOD *meth = TLS_client_method();
    SSL_CTX *ctx = SSL_CTX_new (meth);
    mSsl = SSL_new (ctx);
    if (!mSsl) {
        mStatus = SslSocketStatus::SSL_FAILED;
        mErrorMessage = "SSL_CTX creation failed";
        return ;
    }

    mSock = SSL_get_fd(mSsl);
    SSL_set_fd(mSsl, s);
    int err = SSL_connect(mSsl);
    if (err <= 0) {
        mStatus = SslSocketStatus::SSL_FAILED;
        mErrorMessage = std::string(ERR_error_string(err, 0));
        return;
    }

    mStatus = SslSocketStatus::SSL_CONNECTED;

    if (mDebugLevel >= 2) {
        std::cout << "[SslSocket::SslSocket] [debud] " << mStatus << std::endl;
    }
}

SslSocket::~SslSocket()
{
    if (mStatus != SslSocketStatus::SSL_FAILED) {
        if (mDebugLevel >= 3) {
            std::cout << "[SslSocket::~SslSocket][debug] shutdown\n";
        }
        SSL_shutdown(mSsl);
    }
}


std::ostream& operator<< (std::ostream &outStream, const SslSocketStatus & sslSocketStatus) {
    switch (sslSocketStatus) {
        case SslSocketStatus::SSL_INIT_STATE: outStream << "SSL_INIT_STATE"; break;
        case SslSocketStatus::SSL_CONNECTED: outStream << "SSL_CONNECTED"; break;
        case SslSocketStatus::SSL_CREATED: outStream << "SSL_CREATED"; break;
        case SslSocketStatus::SSL_RECEIVIED: outStream << "SSL_RECEIVIED"; break;
        case SslSocketStatus::SSL_SENT: outStream << "SSL_SENT"; break;
        case SslSocketStatus::SSL_FAILED: outStream << "SSL_FAILED"; break;
    }
    return outStream;
}


