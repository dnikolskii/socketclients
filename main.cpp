﻿#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <thread>
#include <future>
#include <chrono>
#include <cstdlib>
#include <random>

#include <opencv2/opencv.hpp>
#include <cpp-base64/base64.h>
#include <json/json.h>

#include <socketclients/cashgateclient.h>
#include <socketclients/stpgateclient.h>

int main(int argc, char *argv[]) {

    std::cout << "[main][usage] cashgateclient [configFileName]\n";

    std::string configFileName {""};
    if (argc >= 1) {
        configFileName = argv[1];
    } else {
        std::cout << "[main][error] configFileName not found\n";
	return 0;
    }
    std::cout << "[main] configFileName = " << configFileName << std::endl;

    // INT
    //std::string cashIp {"192.162.244.17"};
    //int cashPort {8091};

    //UAT
    std::string cashIp {"192.162.244.71"}; // capi3.weawer.ru
    int cashPort {8090};
    std::string stpIp {"192.162.244.76"}; // sapi3.weawer.ru
    int stpPort {8090};
    std::string merchantId {"X5"};
    std::string storeId {"X5"};
    std::string cashId {"1"};
    std::string deviceId {"1"};
    std::string token {"9b557738-37f2-4024-a193-64b60dc75976"};
    int debugLevel {2};
    int sslSocketdebugLevel {1};
    std::string faceJpg {"dmitry.jpg"};

    // new order
    std::string merchantCashierId {"33"};
    std::string merchantCashier {"Иванов Иван Иванович"};
    double newOrderTotal {5.2};
    double newOrderTotalOrigin {5.2};

    std::string purchaseId {"12345678-9abc-1234-5678-000000000001"};
    std::string purchaseProductId {"888"};
    std::string purchaseName {"Coffe Y"};
    int purchaseQuantity {1};
    double purchaseAmount {5.2};
    double purchasePrice {5.2};
    double purchaseDiscount {0.0};
    double purchaseMerchantDiscount {0.0};
    std::string purchaseUnit {"PCE"};

    // close order
    double amountToClose {5.2};

    try {
        std::ifstream configFile(configFileName);
        std::stringstream ss;
        ss << configFile.rdbuf();
        std::string configString = ss.str();

        JSONCPP_STRING jsonErr;
        Json::Value root;
        Json::CharReaderBuilder builder;
        const std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
        bool ok = reader->parse(configString.c_str(), configString.c_str() + configString.length(), &root, &jsonErr);
        if (!ok) {
            std::cout << "[main][Error] " << jsonErr << std::endl;
            return 0;
        }

        // socket clients settings
        cashIp = root["cashIp"].asString(); // capi3.weawer.ru
        cashPort = std::stoi(root["cashPort"].asString());
        stpIp = root["stpIp"].asString(); // sapi3.weawer.ru
        stpPort = std::stoi(root["stpPort"].asString());
        merchantId = root["merchantId"].asString();
        storeId = root["storeId"].asString();
        cashId = root["cashId"].asString();
        deviceId = root["deviceId"].asString();
        token = root["token"].asString();
        debugLevel = std::stoi(root["debugLevel"].asString());
        sslSocketdebugLevel = std::stoi(root["sslSocketDebugLevel"].asString());
        faceJpg = root["faceJpg"].asString();

        // new order
        merchantCashierId = root["merchantCashierId"].asString();
        merchantCashier = root["merchantCashier"].asString();
        newOrderTotal = std::stod(root["newOrderTotal"].asString());
        newOrderTotalOrigin = std::stod(root["newOrderTotalOrigin"].asString());
        purchaseId = root["purchaseId"].asString();
        purchaseProductId = root["purchaseProductId"].asString();
        purchaseName = root["purchaseName"].asString();
        purchaseQuantity = std::stoi(root["purchaseQuantity"].asString());
        purchaseAmount = std::stod(root["purchaseAmount"].asString());
        purchasePrice = std::stod(root["purchasePrice"].asString());
        purchaseDiscount = std::stod(root["purchaseDiscount"].asString());
        purchaseMerchantDiscount = std::stod(root["purchaseMerchantDiscount"].asString());
        purchaseUnit = root["purchaseUnit"].asString();

        // close order
        amountToClose = std::stod(root["amountToClose"].asString());

    } catch (std::exception & e) {
        std::cout << "[main] json exeption: " << e.what() << std::endl;
        std::cout << "[main][warning] use harcode params\n";
    }

    std::cout << "[main] Json config:\n";
    std::cout << "    cashIp " << cashIp << std::endl;
    std::cout << "    cashPort " << cashPort << std::endl;
    std::cout << "    stpIp " << stpIp << std::endl;
    std::cout << "    stpPort " << stpPort << std::endl;
    std::cout << "    merchantId " << merchantId << std::endl;
    std::cout << "    storeId " << storeId << std::endl;
    std::cout << "    cashId " << cashId << std::endl;
    std::cout << "    deviceId " << deviceId << std::endl;
    std::cout << "    token " << token << std::endl;
    std::cout << "    debugLevel " << debugLevel << std::endl;
    std::cout << "    sslSocketdebugLevel " << sslSocketdebugLevel << std::endl;
    std::cout << "    faceJpg " << faceJpg << std::endl;
    std::cout << "    merchantCashierId = " << merchantCashierId << std::endl;
    std::cout << "    newOrderTotal = " << newOrderTotal << std::endl;
    std::cout << "    newOrderTotalOrigin = " << newOrderTotalOrigin << std::endl;
    std::cout << "    purchaseId = " << purchaseId << std::endl;
    std::cout << "    purchaseProductId = " << purchaseProductId << std::endl;
    std::cout << "    purchaseName = " << purchaseName << std::endl;
    std::cout << "    purchaseQuantity = " << purchaseQuantity << std::endl;
    std::cout << "    purchaseAmount = " << purchaseAmount << std::endl;
    std::cout << "    purchasePrice = " << purchasePrice << std::endl;
    std::cout << "    purchaseDiscount = " << purchaseDiscount << std::endl;
    std::cout << "    purchaseMerchantDiscount = " << purchaseMerchantDiscount << std::endl;
    std::cout << "    purchaseUnit = " << purchaseUnit << std::endl;
    std::cout << "    amountToClose = " << amountToClose << std::endl;

    //cli_create.setSslSocketRcvTimeout(30);
    //cli_create.setSslSocketSndTimeout(30);

    std::string orderIdForPayment {""};

    srand(time(NULL));

    std::cout << "[main] Create order\n";
    {
        static std::random_device rd;
        static std::mt19937 gen(rd());
        gen.seed(time(0));
        static std::uniform_int_distribution<> dis(0, 15);

        std::stringstream sessionCode;
        int i, j;
        sessionCode << std::hex;
        for (i=0; i<8; i++) {
            sessionCode << dis(gen);
        }

        for (j =0; j<3; ++j) {
            sessionCode << "-";
            for (i=0; i<4; i++) {
                sessionCode << dis(gen);
            }
        }
        sessionCode << "-";
        for (i=0; i<12; i++) {
            sessionCode << dis(gen);
        }

        int orderIdRand = rand();
        int orderNumberRand = rand();
        extsocks::NewOrder newOrder;
        newOrder.set_sessioncode(sessionCode.str());
        newOrder.set_merchantorderid(std::to_string(orderIdRand));
        newOrder.set_merchantordernumber(std::to_string(orderNumberRand));
        newOrder.set_merchantcashierid(merchantCashierId);
        newOrder.set_merchantcashier(merchantCashier);
        std::time_t currentTimeSecs = std::time(nullptr); //seconds since the Epoch
        newOrder.set_saledate(currentTimeSecs * 1000);
        newOrder.set_total(newOrderTotal);
        newOrder.set_totaloriginal(newOrderTotalOrigin);

        extsocks::Purchase *purchase = newOrder.add_purchases();
        purchase->set_id(purchaseId);// Идентификатор товара в системе SWiP.
        purchase->set_productid(purchaseProductId); // Артикул товара.
        purchase->set_name(purchaseName);
        purchase->set_quantity(purchaseQuantity); // количество товара
        purchase->set_amount(purchaseAmount); // стоимость price  * amount
        purchase->set_price(purchasePrice); // цена
        purchase->set_discount(purchaseDiscount);
        purchase->set_merchantdiscount(purchaseMerchantDiscount);
        purchase->set_unit(purchaseUnit); // PCE + KGM

        std::cout << "[main] NewOrder:\n";
        std::cout << newOrder.Utf8DebugString();

        CashGateClient cli_create;
        SocketClientSetting cashGateSetting;
        cashGateSetting.setDebugLevel(2);
        cashGateSetting.setSslSocketDebugLevel(2);
        cashGateSetting.setSslSocketSndTimeout(30);
        cashGateSetting.setSslSocketRcvTimeout(30);
        cashGateSetting.setPort(cashPort);
        cashGateSetting.setIp(cashIp.c_str());
        cli_create.setSocketClientSetting(cashGateSetting);

        MerchantSetting merchantSetting;
        merchantSetting.setMerchantId(merchantId);
        merchantSetting.setToken(token);
        merchantSetting.setStoreId(storeId);
        merchantSetting.setCashId(cashId);
        cli_create.setMerchantSetting(merchantSetting);
        cli_create.connect();
        SocketClientStatus stat = cli_create.getStatus();
        std::cout << "[main] cli_create.getStatus = " << stat << std::endl;

        std::future<bool> the_result_create = std::async(&CashGateClient::createOrder, &cli_create, newOrder);
        stat = cli_create.getStatus();
        while (stat != SocketClientStatus::CASH_ORDER_CREATED) {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            stat = cli_create.getStatus();
            std::cout << "[main] cli_create.getStatus = " << stat << std::endl;
            if (stat == SocketClientStatus::SOCKET_CLIENT_FAILED) break;
        }

        extsocks::Order orderCash;
        orderCash = cli_create.getOrder();
        std::cout << "[mai] getOrder()\n";
        std::cout << orderCash.Utf8DebugString();
        orderIdForPayment = orderCash.id();

        bool cliCreateResult = the_result_create.get();
        std::cout << "[main] CashGateClient::createOrder() return is " << cliCreateResult << std::endl;

        if (!cliCreateResult) return 0;
    }

    std::cout << "\n ---- [main] start pay ----\n";
    {
        std::string encoded;
        try {
            cv::Mat face, faceRaw;
            faceRaw = cv::imread(faceJpg, 1);
            cv::resize(faceRaw, face, cv::Size(224, 224));
            std::vector<unsigned char> buf;
            cv::imencode(".jpg", face, buf);
            unsigned char *enc_msg = new unsigned char[buf.size()];
            for (int i = 0; i < buf.size(); i++) enc_msg[i] = buf[i];
            encoded = base64_encode(enc_msg, buf.size());
            std::cout << "faceJpg: ";
            for (int j=0; j<25; ++j) std::cout << encoded[j] ;
            std::cout << std::endl;
        } catch (std::exception & e) {
            std::cout << "[main][error] " << e.what() << std::endl;
        }

        StpGateClient stpGateClient;
        SocketClientSetting stpGateSetting;
        stpGateSetting.setDebugLevel(debugLevel);
        stpGateSetting.setSslSocketDebugLevel(sslSocketdebugLevel);
        stpGateSetting.setSslSocketRcvTimeout(30);
        stpGateSetting.setSslSocketSndTimeout(30);
        stpGateSetting.setIp(stpIp);
        stpGateSetting.setPort(stpPort);
        stpGateClient.setSocketClientSetting(stpGateSetting);

        MerchantSetting merchantSetting;
        merchantSetting.setMerchantId(merchantId);
        merchantSetting.setToken(token);
        merchantSetting.setStoreId(storeId);
        merchantSetting.setCashId(cashId);
        merchantSetting.setDeviceId(deviceId);
        stpGateClient.setMerchantSetting(merchantSetting);
        stpGateClient.connect();
        SocketClientStatus stat = stpGateClient.getStatus();
        std::cout << "[main] stpGateClient.getStatus = " << stat << std::endl;

        std::future<bool> the_stp_result = std::async(&StpGateClient::payByFace, &stpGateClient,
                                                      orderIdForPayment, encoded);

        stat = stpGateClient.getStatus();
        std::cout << "[main] STPGatwayStatus = " << stat << std::endl;
        bool doGetPoints = true;
        while(stat != SocketClientStatus::STP_SUCCESSFULLY_PAID && stat != SocketClientStatus::SOCKET_CLIENT_FAILED)
        {
            if (stat == SocketClientStatus::STP_WAIT_POINTS && doGetPoints) {

                std::cout << "[main] enter points: " << std::flush;
                int tmp;
                std::cin >> tmp;
                stpGateClient.setPointsForPayment(tmp);
                doGetPoints = false;
                std::this_thread::sleep_for(std::chrono::seconds(3));
            }
            if (stat == SocketClientStatus::STP_WAIT_PINCODE || stat == SocketClientStatus::STP_INCORRECT_PINCODE) {
                std::string pin;
                std::cout << "[main] pin: ";
                std::cin >> pin;
                stpGateClient.setPinCode(pin);
                std::this_thread::sleep_for(std::chrono::seconds(3));
            }
            stat = stpGateClient.getStatus();
            std::cout << "[main] STPGatwayStatus  = " << stat << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        std::cout << "[main] sleep 1s ... " << std::flush;
        std::this_thread::sleep_for(std::chrono::seconds(3));

        bool stpResult = the_stp_result.get();
        std::cout << "[main] StpGateClient::payByFace return is " << stpResult << std::endl;
        if (!stpResult) return 0;

        extsocks::Order paidOrder = stpGateClient.getOrder();
        std::cout << "[main] paidOrder:\n";
        std::cout << "id = " << paidOrder.id() << std::endl;
    }


    std::cout << "[main] close order " << orderIdForPayment << std::endl;
    {
        CashGateClient closeClient;
        SocketClientSetting cashGateSetting;
        cashGateSetting.setDebugLevel(2);
        cashGateSetting.setSslSocketDebugLevel(2);
        cashGateSetting.setSslSocketSndTimeout(30);
        cashGateSetting.setSslSocketRcvTimeout(30);
        cashGateSetting.setPort(cashPort);
        cashGateSetting.setIp(cashIp.c_str());
        closeClient.setSocketClientSetting(cashGateSetting);
        MerchantSetting merchantSetting;
        merchantSetting.setMerchantId(merchantId);
        merchantSetting.setToken(token);
        merchantSetting.setStoreId(storeId);
        merchantSetting.setCashId(cashId);
        closeClient.setMerchantSetting(merchantSetting);
        closeClient.connect();
        SocketClientStatus stat = closeClient.getStatus();
        std::cout << "[main] cli_close.getStatus = " << stat << std::endl;
        if (stat == SocketClientStatus::SOCKET_CLIENT_FAILED) {
            std::cout << "[main] cli_close error: " << closeClient.getErrorMessage() << std::endl;
        }
        //closeClient.setOrderToClose(orderIdForPayment);
        //closeClient.setAmountToClose(amountToClose);
        std::future<bool> the_result_close = std::async(&CashGateClient::closeOrder, &closeClient,
                                                        orderIdForPayment, amountToClose);
        stat = closeClient.getStatus();
        std::cout << "[main] CashGateClient::closeOrder() return is " << the_result_close.get() << std::endl;
    }
    return 0;
}
