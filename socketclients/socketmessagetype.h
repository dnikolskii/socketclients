#ifndef SOCKETMESSGAGETYPEEXTSOCKSNDN202009_H
#define SOCKETMESSGAGETYPEEXTSOCKSNDN202009_H

#include <iostream>


enum class SocketMessageType: int {

    SOCKET_TYPE_EMPTY = -1,

    // --- COMMON ---
    HEART_BEAT_REQUEST = 0, // message HeartBeatRequest
    ERROR_MESSAGE = 1, // message ErrorMessage
    HEART_BEAT_RESPONSE = 2, // message HeartbeatResponse
    LOGIN_RESPONSE_MESSAGE = 11, // message LoginResponseMessage

    // --- STP ---
    LOGIN_REQUEST_MESSAGE = 10, // message LoginRequestMessage
    SET_RATING_REQUEST_MESSAGE = 20, // message SetRatingRequestMessage
    SET_RATING_RESPONSE_MESSAGE = 21, // message SetRatingResponseMessage
    PINCODE_PROCESS_REQUEST_MESSAGE = 30, // message PincodeProcessRequestMessage
    PAY_WITH_POINTS_REQUEST_MESSAGE = 40, // message PayWithPointsRequestMessage
    SEND_WARP_FRAME_REQUEST_MESSAGE = 50, // message SendWarpFrameRequestMessageage
    SEND_FRAME_REQUEST_MESSAGE = 60, // message SendFrameRequestMessage
    STP_PAYMENT_RESPONSE_MESSAGE = 61, // message StpPaymentResponseMessage
    RAW_FRAME_REQUEST_MESSAGE = 70, // message RawFrameRequestMessage
    RAW_FRAME_RESPONSE_MESSAGE = 71, // message RawFrameResponseMessfigRequestMessage
    CONFIG_REQUEST_MESSAGE = 80, // message Con
    CONFIG_RESPONSE_MESSAGE = 81, // message ConfigResponseMessage
    ACTIVATE_DEVICE_REQUEST_MESSAGE = 90, // message ActivateDeviceRequestMessage
    ORDER_STATUS_UPDATE_MESSAGE = 100, // message OrderStatusUpdatedMessage
    IR_IMAGE_REQUEST_MESSAGE = 110, // message IrImageRequestMessage
    IR_IMAGE_RESPONSE_MESSAGE = 111, // message IrImageResponseMessage
    DEVICE_LOG_MESSAGE = 112, //    message DeviceLogMessage
    UPDATE_AVAILABLE_MESSAGE = 120, //  message UpdateAvailableMessage
    ROLL_BACK_MESSAGE = 121, // message RollBackMessage
    IDENTIFICATION_REQUEST = 130, // message IdentificationRequest
    IDENTIFICATION_RESPONSE = 131, // message IdentificationResponse
    IDENTIFICATION_PINCODE_REQUEST = 132, // message IdentificationPincodeRequest

    // --- CASH ---
    CASH_LOGIN_REQUEST = 12, // 12  CashLoginRequest
    CASH_CREATE_ORDER_REQUEST = 200, // message CashCreateOrderRequest
    CASH_CREATE_ORDER_RESPONSE = 201, //   message CashCreateOrderResponse
    CASH_GET_ORDER_REQUEST = 210, //  message CashGetOrderRequest
    CASH_GET_ORDER_RESPONSE = 211, //  message CashGetOrderResponse
    CASH_GET_ORDER_LIST_REQUEST = 220, //  message CashGetOrderListRequest
    CASH_GET_ORDER_LIST_RESPONSE = 221, //`  message CashGetOrderListResponse
    CASH_CLOSE_ORDER_REQUEST = 230, //  message CashCloseOrderRequest
    CASH_CLOSE_ORDER_RESPONSE = 231, //  message CashCloseOrderResponse
    CASH_ORDER_STATUS_UPDATED = 240, //  message CashOrderStatusUpdated
    CASH_SAVE_SETTING_REQUEST = 250, //  message CashSaveSettingRequest
    CASH_SAVE_SETTING_RESPONSE = 251, //  message CashSaveSettingResponse
    CASH_GET_SETTING_REQUEST = 260, //  message CashGetSettingRequest
    CASH_GET_SETTING_RESPONSE = 261, //  message CashGetSettingResponse
    CASH_ORDER_APPLY_MERCHANT_DISCOUNT_REQUEST = 270, //  message CashOrderApplyMerchantDiscountRequest
    CASH_ORDER_APPLY_MERCHANT_DISCOUNT_RESPONSE = 271 //  message CashOrderApplyMerchantDiscountResponse
};


std::ostream& operator<< (std::ostream &outStream, const SocketMessageType &socketMessageType);

#endif
