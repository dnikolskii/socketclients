syntax = "proto3";
import "google/protobuf/empty.proto";
import "google/protobuf/wrappers.proto";

option java_multiple_files = true;
package ru.smartwallet.extsocks;

option optimize_for = SPEED;

message Error {
    int32 code = 1;
    string description = 2;
}

message Purchase {
    string id = 1;
    string productId = 2;
    string name = 3;
    double quantity = 4;
    double amount = 5;
    double price = 6;
    double discount = 7;
    double merchantDiscount = 8;
    string unit = 9;
}

enum PaymentWay {
    PW_EMPTY = 0;
    APP = 1;
    STP = 2;
}

enum OrderStatus {
    OS_EMPTY = 0;
    OPEN = 1;
    CLOSED = 2;
    LOYALTY_CALCULATION = 3;
}

enum PaymentMethod {
    PM_EMPTY = 0;
    CARD = 1;
    CREDIT = 2;
    CASH = 3;
}

message Order {
    string sessionCode = 1;
    string shortCode = 2;
    string id = 3;
    string customerId = 4;
    string merchantId = 5;
    string merchantName = 6;
    string merchantProfileImageId = 7;
    string merchantCategory = 8;
    string merchantOrderId = 9;
    string merchantOrderNumber = 10;
    string merchantStore = 11;
    string merchantCash = 12;
    string merchantCashierId = 13;
    string merchantCashier = 14;
    int64 saleDate = 15;
    double total = 16;
    double totalOriginal = 17;
    double cashback = 18;
    double discount = 19;
    double swipDiscount = 20;
    repeated Purchase purchases = 21;
    double paid = 22;
    double unconfirmed = 23;
    double closeAmount = 24;
    int32 availablePoints = 25;
    string error = 26;
    double pointsRate = 27;
    double pointsAsDiscount = 28;
    int32 awardPoints = 29;
    int32 withdrawPoints = 30;
    PaymentWay paymentWay = 31;
    OrderStatus orderStatus = 32;
    PaymentMethod paymentMethod = 33;
    int32 apiVersion = 34;
    string otp = 35;
    string slip = 36;
    string rrn = 37;
    string authorizationCode = 38;
    string terminalNo = 39;
    string pan = 40;
    double merchantDiscount = 41;
    string discountCard = 42;
}

message Customer {
    string id = 1;
    bool adult = 2;
    string extId = 3;
}

enum RefundStatus {
    RS_EMPTY = 0;
    SUCCESS = 1;
    FAIL = 2;
}

//-------------------------------------- Common --------------------------------------//

// 0
message HeartBeatRequest {
    string requestId = 1;
    int64 deviceId = 2;
}

// 1
message ErrorMessage {
    string requestId = 1;
    string description = 2;
    int32 errorCode = 3;
}

// 2
message HeartbeatResponse {
    string requestId = 1;
}

enum DeviceType {
    SCC = 0; // self checkout cash
    STPD = 1; // Selfie To Pay Device
}

// 10
message LoginRequestMessage {
    string requestId = 1;
    string username = 2;
    string storeId = 4;
    string cashId = 5;
    string token = 6;
    string version = 7;
    string deviceId = 8;
    DeviceType type = 9;
}

message UpdateInfo {
    string version = 1;
    string url = 2;
    bool force = 3;
}

// 11
message LoginResponseMessage {
    string requestId = 1;
    UpdateInfo updateInfo = 2;
}

// 12
message CashLoginRequest {
    string merchantId = 1;
    string token = 2;
    string storeId = 3;
    string cashId = 4;
    string requestId = 5;
}

//-------------------------------------- STP --------------------------------------//

// 20
message SetRatingRequestMessage {
    string requestId = 1;
    string orderID = 2;
    int32 rating = 3;
}

// 21
message SetRatingResponseMessage {
    string requestId = 1;
}

// 30
message PincodeProcessRequestMessage {
    string requestId = 1;
    string pincode = 2;
    string stpPaymentId = 3;
    string deviceId = 4;
}

// 40
message PayWithPointsRequestMessage {
    string requestId = 1;
    string stpPaymentId = 2;
    int32 withdrawPointsAmount = 3;
    string deviceId = 4;
}

// 50
message SendWarpFrameRequestMessage {
    string requestId = 1;
    bytes content = 2;
    string contentType = 3;
    string fileName = 4;
    string identity = 5;
    string orderId = 6;
    string deviceId = 7;
}

// 60
message SendFrameRequestMessage {
    string requestId = 1;
    string rawFrame = 2;
    string warpFrame = 3;
    string orderId = 4;
    string deviceId = 5;
}

// 61
message StpPaymentResponseMessage {
    string requestId = 1;
    string stpPaymentId = 2;
    int32 code = 3;
    string description = 4;
    Order order = 5;
    string customerId = 6;
}

// 70
message RawFrameRequestMessage {
    string requestId = 1;
    bytes content = 2;
    string contentType = 3;
    string fileName = 4;
}

// 71
message RawFrameResponseMessage {
    string requestId = 1;
    bytes content = 2;
    string contentType = 3;
    string fileName = 4;
}

// 80
message ConfigRequestMessage {
    string requestId = 1;
    string deviceId = 2;
}

// 81
message ConfigResponseMessage {
    string requestId = 1;
    string deviceId = 2;
    int64 revision = 3;
    string stpLogin = 4;
    string stpPass = 5;
    int64 updatedAt = 6;
    string cashId = 7;
    string storeId = 8;
    string merchantId = 9;
    string authToken = 10;
    string orderUrl = 11;
    string framesUrl = 12;
    string pincodeUrl = 13;
    string uiInitSplashScreen = 14;
    bool uiSkipPageAccept = 15;
    bool uiReportShowRating = 16;
    string uiLanguage = 17;
    bool uiReportDetailedErrors = 18;
    bool uiCaptureShowFrame = 19;
    bool uiFullscreen = 20;
    int32 uiForceWidth = 21;
    int32 uiForceHeight = 22;
    bool uiForceFrameless = 23;
    bool adsEnable = 24;
    string adsPath = 25;
    int32 adsDuration = 26;
    int32 adsInterval = 27;
    bool swipInsecure = 28;
    bool swipVerbose = 29;
    string swipCustomheader = 30;
    string httpUser = 31;
    string httpPassword = 32;
    int32 httpPort = 33;
    bool httpsEnabled = 34;
    string httpsCertificatePath = 35;
    string httpsPrivateKeyPath = 36;
    bool proxyEnable = 37;
    string proxyHost = 38;
    int32 proxyPort = 39;
    string proxyUsername = 40;
    string proxyPassword = 41;
    int32 timeout = 42;
    string cameraMode = 43;
    string cameraVirtualImage = 44;
    double detectionMinimumSharpness = 45;
    double detectionMinimumExposureQuality = 46;
    double maximumRotation = 47;
    double detectionMinimumMargin = 48;
    bool detectionDebug = 49;
    string fixmeStrAcceptSpecialOffer = 50;
}

// 90
message ActivateDeviceRequestMessage {
    Order order = 2;
}

// 100
message OrderStatusUpdatedMessage {
    Order order = 3;
}

// 110
message IrImageRequestMessage {
    string requestId = 1;
    string customerId = 2;
    bytes content = 3;
}

// 111
message IrImageResponseMessage {
    string requestId = 1;
}

// 112
message DeviceLogMessage {
    string deviceId = 1;
    string message = 2;
}

// 120
message UpdateAvailableMessage {
    string requestId = 1;
    UpdateInfo updateInfo = 2;
}

// 121
message RollBackMessage {
    string requestId = 1;
}

// 130
message IdentificationRequest {
    string requestId = 1;
    string frame = 2;
}

// 131
message IdentificationResponse {
    string requestId = 1;
    Customer customer = 2;
    Error error = 3;
    string attemptId = 4;
}

// 132
message IdentificationPincodeRequest {
    string requestId = 1;
    string attemptId = 2;
    string pin = 3;
}

// 140
message CalculateLoyaltyRequest {
    string requestId = 1;
    string orderId = 2;
    string customerId = 3;
    uint32 pointsToWithdraw = 4;
}

// 141
message CalculateLoyaltyResponse {
    string requestId = 1;
    Order order = 2;
}

// 150
message PayOrderRequest {
    string requestId = 1;
    string customerId = 2;
    string orderId = 3;
    double amountToPay = 4;
}

// 151
message PayOrderResponse {
    string requestId = 1;
}

//-------------------------------------- Cash --------------------------------------//

message NewOrder {
    string sessionCode = 1;
    string merchantOrderId = 2;
    string merchantOrderNumber = 3;
    string merchantCashierId = 4;
    string merchantCashier = 5;
    sint64 saleDate = 6;
    double total = 7;
    double totalOriginal = 8;
    repeated Purchase purchases = 9;
}

// 200
message CashCreateOrderRequest {
    string requestId = 1;
    NewOrder newOrder = 2;
}

// 201
message CashCreateOrderResponse {
    string requestId = 1;
    Order order = 2;
}

// 210
message CashGetOrderRequest {
    string requestId = 1;
    string orderId = 2;
}

// 211
message CashGetOrderResponse {
    string requestId = 1;
    Order order = 2;
}

// 220
message CashGetOrderListRequest {
    string requestId = 1;
    google.protobuf.UInt64Value from = 2;
    google.protobuf.UInt64Value to = 3;
}

// 221
message CashGetOrderListResponse {
    string requestId = 1;
    repeated Order orders = 2;
}

// 230
message CashCloseOrderRequest {
    string requestId = 1;
    string orderId = 2;
    double amountToClose = 3;
}

// 231
message CashCloseOrderResponse {
    string requestId = 1;
    Order order = 2;
}

// 240
message CashOrderStatusUpdated {
    string requestId = 1;
    string orderId = 2;
    double totalOriginal = 3;
    double total = 4;
    double paid = 5;
    double unconfirmed = 6;
    double refunded = 7;
    OrderStatus orderStatus = 8;
    string discountCard = 9;
    double swipDiscount = 10;
}

// 250
message CashSaveSettingRequest {
    string requestId = 1;
    string settingName = 2;
    string settingValue = 3;
}

// 251
message CashSaveSettingResponse {
    string requestId = 1;
}

// 260
message CashGetSettingRequest {
    string requestId = 1;
    string settingName = 2;
}

// 261
message CashGetSettingResponse {
    string requestId = 1;
    string settingName = 2;
    string settingValue = 3;
}

// 270
message CashOrderApplyMerchantDiscountRequest {
    string requestId = 1;
    string orderId = 2;
    double merchantDiscount = 3;
    double total = 4;
    repeated Purchase purchases = 5;
}

// 271
message CashOrderApplyMerchantDiscountResponse {
    string requestId = 1;
    Order order = 2;
}

// 280
message RefundRequest {
    string requestId = 1;
    string merchantOrderId = 2;
    double amount = 3;
}

// 281
message RefundResponse {
    string requestId = 1;
    RefundStatus status = 2;
    string description = 3;
    uint32 code = 4;
    string slip = 5;
}


//-------------------------------------- POS --------------------------------------//
// 400
message PosRegistrationRequest {
    string requestId = 1;
    string email = 2;
}

// 401
message PosRegistrationResponse {
    string requestId = 1;
}

// 402
message PosVerificationCodeRequest {
    string requestId = 1;
    string code = 2;
}

// 403
message PosVerificationCodeResponse {
    string requestId = 1;
    string token = 2;
    string merchantId = 3;
    string storeId = 4;
}
