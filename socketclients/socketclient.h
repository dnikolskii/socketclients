#ifndef SOCKETCLIENTEXTSOCKSNDN_202007_H
#define SOCKETCLIENTEXTSOCKSNDN_202007_H

#include <sstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <mutex>
#include <variant>
#include <random>

#include <sslsocket/sslsocket.h>
#include <socketclients/socketmessagetype.h>
#include <extsocks-protocol.pb.h>

using namespace ru::smartwallet;


typedef std::variant<extsocks::LoginResponseMessage, extsocks::StpPaymentResponseMessage,
    extsocks::RawFrameRequestMessage, extsocks::OrderStatusUpdatedMessage, extsocks::CashCreateOrderResponse,
    extsocks::CashCloseOrderResponse, extsocks::IdentificationResponse> AnySocketMessagesResponse;


enum class SocketClientStatus : int {
    // --- Common ---
    SOCKET_CLIENT_INIT = 0,
    SOCKET_CLIENT_SSL_CONNECTED = 1,
    SOCKET_CLIENT_SSL_NO_CONNECTION = 2,
    SOCKET_CLIENT_FAILED = 3,
    SOCKET_CLIENT_LOGGED_IN = 4,
    // --- Cash Gate Client ---
    CASH_PROCESSING_STARTED = 100,
    CASH_LOGGED_IN = 101,
    CASH_ORDER_CREATED = 102,
    CASH_ORDER_CLOSED = 103,
    // --- STP Client ---
    STP_PROCESSING_STARTED = 200,
    STP_PAYMENT_MESSAGE_RECEIVED = 202,
    STP_ORDER_UPDATED_MESSAGE_RECEIVED = 203,
    STP_WAIT_PINCODE = 204,
    STP_INCORRECT_PINCODE = 205,
    STP_WAIT_POINTS = 206,
    STP_INCORRECT_POINTS = 207,
    STP_WAIT_ORDER = 208,
    STP_SUCCESSFULLY_PAID = 209
};


class SocketClientSetting {
public:
    void setIp(const std::string & ip) {mIp = ip;}
    void setPort(int port) {mPort = port;}
    void setDebugLevel(int debugLevel) {mDebugLevel = debugLevel;}
    void setSslSocketRcvTimeout(int rcvTimeout) {mSslSocketRcvTimeout = rcvTimeout;}
    void setSslSocketSndTimeout(int sndTimeout) {mSslSocketSndTimeout = sndTimeout;}
    void setSslSocketDebugLevel(int debugLevel) {mSslSocketDebugLevel = debugLevel;}

    std::string getIp() const {return mIp;}
    int getPort() const {return mPort;}
    int getDebugLevel() const {return mDebugLevel;}
    int getSslSocketRcvTimeout() const {return mSslSocketRcvTimeout;}
    int getSslSocketSndTimeout() const {return mSslSocketSndTimeout;}
    int getSslSocketDebugLevel() const {return mSslSocketDebugLevel;}

private:
    std::string mIp {""};
    int mPort {0};
    int mDebugLevel {2};
    int mSslSocketRcvTimeout {30};
    int mSslSocketSndTimeout {30};
    int mSslSocketDebugLevel {1};
};

std::ostream& operator<< (std::ostream &outStream, const SocketClientSetting & socketClientSetting);


class MerchantSetting {
public:
    void setMerchantId (const std::string & merchantId) {mMerchantId = merchantId;}
    void setStoreId (const std::string & storeId) {mStoreId = storeId;}
    void setCashId (const std::string & cashId) {mCashId = cashId;}
    void setToken (const std::string & token) {mToken = token;}
    void setDeviceId (const std::string & deviceId) {mDeviceId = deviceId;}

    std::string getMerchantId() const {return mMerchantId;}
    std::string getStoreId() const {return mStoreId;}
    std::string getCashId() const {return mCashId;}
    std::string getToken() const {return mToken;}
    std::string getDeviceId () const {return mDeviceId;}

private:
    std::string mMerchantId {""};
    std::string mStoreId {""};
    std::string mCashId {""};
    std::string mToken {""};
    std::string mDeviceId {"SWiPSDKDevice"};
};

std::ostream& operator<< (std::ostream &outStream, const MerchantSetting & merchantSetting);


class SocketClient {
public:
    // --- Common functional ---
    SocketClient();
    //void connect(const std::string & ip, int port);
    void connect();
    SocketClientStatus getStatus() const;
    std::string getErrorMessage() const;
    void setSocketClientSetting(const SocketClientSetting & value) {mSocketClientSetting = value;}
    void setMerchantSetting(const MerchantSetting & value) {mMerchantSetting = value;}
    void testConnectionToSTPGate();

    // --- Order ---
    extsocks::Order getOrder() const {std::unique_lock<std::mutex> lock(mOrderMutex); return mOrder;};

    virtual ~SocketClient();

protected:

    // Common state-variables
    std::string mRequestId {""};
    SocketClientSetting mSocketClientSetting;
    MerchantSetting mMerchantSetting;

    mutable std::mutex mOrderMutex;
    extsocks::Order mOrder;

    // Raw Request-Response
    void mRequest (const std::string & data, int messageId);
    AnySocketMessagesResponse mResponse(const SocketMessageType & socketMessageType);

    // Common Request
    void mLoginRequestMessage(const std::string & requestId, const std::string & userName, const std::string & storeId,
                              const std::string & cashId, const std::string & token); // 10


    std::string mSocketMessage {""};
    SocketMessageType mSocketMessageType {SocketMessageType::SOCKET_TYPE_EMPTY};

    mutable std::mutex mStatusMutex;
    SocketClientStatus mStatus {SocketClientStatus::SOCKET_CLIENT_INIT};
    std::string mErrorMessage {""};

    SslSocket *mSslSocket = nullptr;

    std::mt19937 mGen;
    double mEpsilon {0.001};
};

std::ostream& operator<< (std::ostream &outStream, const SocketClientStatus & socketClientStatus);
#endif