﻿#ifndef STPGATWAYCLIENTNDN_202007_H
#define STPGATWAYCLIENTNDN_202007_H

#include <sstream>
#include <mutex>
#include <thread>
#include <chrono>
#include <stdio.h>
#include <string>
#include <iostream>
#include <iomanip>
#include <ctime>
#include <variant>
#include <set>

#include <extsocks-protocol.pb.h>
#include <socketclients/socketclient.h>

using namespace ru::smartwallet;


class StpGateClient: public SocketClient
{
public:
    StpGateClient();
    int getAvailablePoints() const { std::unique_lock<std::mutex> lock(mStatusMutex); return mAvailablePoints;}
    void setPinCode(const std::string & pinCode) {
        std::unique_lock<std::mutex> lock(mPinCodeMutex);
        mPinCode = pinCode;
    }
    short getAttemptPincode() {return mAttemptPincode;}

    void setPointsForPayment(int points) {
        std::unique_lock<std::mutex> lock(mPointsForPaymentMutex);
        mPointsForPayment = points;
    }
    short getAttemptPoints() {return mAttemptPoints;}
    bool payByFace(const std::string & orderId, const std::string & face);
    void breakPayment() {mIsBreakPayment = true;}

    bool IdentificationByFace(const std::string & requestId, const std::string & face);

    virtual ~StpGateClient(){}


private:
    int mAvailablePoints {0};
    std::atomic<short> mAttemptPincode;
    std::atomic<short> mAttemptPoints;

    // Request
    void mSendFrameRequestMessage (const std::string & requestId, const std::string & warpFrame,
                                   const std::string & orderId, const std::string & deviceId); // 60
    void mPincodeProcessRequestMessage(const std::string & requestId, const std::string & pincode,
                                       const std::string & stpPaymentId, const std::string & deviceId); // 30
    void mPayWithPointsRequestMessage(const std::string & requestId, const std::string & stpPaymentId,
                                      int points, const std::string & deviceId); // 40

    void mIdentificationRequest (const std::string & requestId, const std::string & warpFrame); // 130

    std::atomic<bool> mIsBreakPayment {false};

    std::string mPinCode {""};
    mutable std::mutex mPinCodeMutex;
    std::string mGetPinCode() {
        std::unique_lock<std::mutex> lock(mPinCodeMutex);
        if (mSocketClientSetting.getDebugLevel() >= 3) {
            if (!mPinCode.empty()) {
                std::cout << "[STPGateWayClient][Debug] GET mPinCode = '" << mPinCode << "'\n";
            }
        }
        return mPinCode;
    }
    void mClearPinCode (){
        std::unique_lock<std::mutex> lock(mPinCodeMutex);
        mPinCode.clear();
        if (mSocketClientSetting.getDebugLevel() >= 3) {
            std::cout << "[STPGatWayClient][Debug] CLEAN mPinCode = '" << mPinCode << "'\n";
        }
    }

    int mPointsForPayment {-1};
    mutable std::mutex mPointsForPaymentMutex;
    int mGetPointsForPayment() {
        std::unique_lock<std::mutex> lock(mPointsForPaymentMutex);
        return mPointsForPayment;
    }
    void mClearPointsForPayment (){
        std::unique_lock<std::mutex> lock(mPinCodeMutex);
        mPointsForPayment = -1;
    }
};


#endif