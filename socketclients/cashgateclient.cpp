﻿#include "cashgateclient.h"



void CashGateClient::testConnectionToCashGate() {
    {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mStatus = SocketClientStatus::CASH_PROCESSING_STARTED;
    }
    mRequestId = std::to_string(mGen());
    mCashLoginRequest(mMerchantSetting.getMerchantId(), mMerchantSetting.getToken(),
                      mMerchantSetting.getStoreId(), mMerchantSetting.getCashId(),mRequestId);
    extsocks::LoginResponseMessage loginResponseMessage = std::get<extsocks::LoginResponseMessage>(
            mResponse(SocketMessageType::LOGIN_RESPONSE_MESSAGE));
    {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mStatus = SocketClientStatus::SOCKET_CLIENT_LOGGED_IN;
    }
}



bool CashGateClient::createOrder(const extsocks::NewOrder & newOrder)
{
    //{
    //    std::unique_lock<std::mutex> lock(mOrderMutex);
    //    mOrder = extsocks::Order();
    //}
    {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mStatus = SocketClientStatus::CASH_PROCESSING_STARTED;
    }
    int debugLevel = mSocketClientSetting.getDebugLevel();

    mRequestId = std::to_string(mGen());

    try {
        mCashLoginRequest(mMerchantSetting.getMerchantId(), mMerchantSetting.getToken(),
                          mMerchantSetting.getStoreId(), mMerchantSetting.getCashId(),
                          mRequestId);
        extsocks::LoginResponseMessage loginResponseMessage = std::get<extsocks::LoginResponseMessage>(
                mResponse(SocketMessageType::LOGIN_RESPONSE_MESSAGE));

        mCashCreateOrderRequest(mRequestId, newOrder);
        extsocks::CashCreateOrderResponse cashCreateOrderResponse = std::get<extsocks::CashCreateOrderResponse>(
                mResponse(SocketMessageType::CASH_CREATE_ORDER_RESPONSE));
        {
            std::unique_lock<std::mutex> lock(mOrderMutex);
            mOrder = cashCreateOrderResponse.order();
        }
        {
            std::unique_lock<std::mutex> lock(mStatusMutex);
            mStatus = SocketClientStatus::CASH_ORDER_CREATED;
        }

    } catch (const std::exception & exct) {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mErrorMessage = exct.what();
        mStatus = SocketClientStatus::SOCKET_CLIENT_FAILED;
        if (debugLevel >= 1) {
            std::cout << "[CashGateClient::createOrder][error] " << mErrorMessage << std::endl;
        }
        return false;
    } catch (...) {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mErrorMessage = "Unhandled pay_by_face exception";
        mStatus = SocketClientStatus::SOCKET_CLIENT_FAILED;
        if (debugLevel >= 1) {
            std::cout << "[CashGateClient::createOrder][error] catch unknow exception" << mErrorMessage << std::endl;
        }
        return false;
    }

    return true;
}


bool CashGateClient::closeOrder(const std::string & orderId, double amountToClose)
{
    //{
    //    std::lock_guard<std::mutex> lock(mOrderMutex);
    //    mOrderId = extsocks::Order();
    //}
    {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mStatus = SocketClientStatus::CASH_PROCESSING_STARTED;
    }

    mRequestId = std::to_string(mGen());
    int debugLevel = mSocketClientSetting.getDebugLevel();

    try {
        mCashLoginRequest(mMerchantSetting.getMerchantId(), mMerchantSetting.getToken(),
                          mMerchantSetting.getStoreId(), mMerchantSetting.getCashId(),
                          mRequestId);
        extsocks::LoginResponseMessage loginResponseMessage = std::get<extsocks::LoginResponseMessage>(
                mResponse(SocketMessageType::LOGIN_RESPONSE_MESSAGE));

        mCashCloseOrderRequest(mRequestId, orderId, amountToClose);
        extsocks::CashCloseOrderResponse cashCloseOrderResponse = std::get<extsocks::CashCloseOrderResponse>(
                mResponse(SocketMessageType::CASH_CLOSE_ORDER_RESPONSE));

        {
            std::unique_lock<std::mutex> lock(mOrderMutex);
            mOrder = cashCloseOrderResponse.order();
        }

        {
            std::unique_lock<std::mutex> lock(mStatusMutex);
            mStatus = SocketClientStatus::CASH_ORDER_CLOSED;
        }


    } catch (const std::exception & exct) {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mErrorMessage = exct.what();
        mStatus = SocketClientStatus::SOCKET_CLIENT_FAILED;
        if (debugLevel >= 1) {
            std::cout << "[CashGateClient::createOrder][error] " << mErrorMessage << std::endl;
        }
        return false;
    } catch (...) {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mErrorMessage = "Unhandled pay_by_face exception";
        mStatus = SocketClientStatus::SOCKET_CLIENT_FAILED;
        if (debugLevel >= 1) {
            std::cout << "[CashGateClient::createOrder][error] catch unknow exception" << mErrorMessage << std::endl;
        }
        return false;
    }

    return true;
}



// --- Request Message ---

void CashGateClient::mCashLoginRequest(std::string merchantId, std::string token, std::string storeId,
                                       std::string cashId, std::string requestId)
{
    int messageId {static_cast<int>(SocketMessageType::CASH_LOGIN_REQUEST)}; // 12
    int debugLevel = mSocketClientSetting.getDebugLevel();
    std::string data;
    extsocks::CashLoginRequest bodyData;
    bodyData.set_merchantid(merchantId);
    bodyData.set_token(token);
    bodyData.set_storeid(storeId);
    bodyData.set_cashid(cashId);
    bodyData.set_requestid(requestId);

    bodyData.SerializeToString(&data);
    if (debugLevel >= 2) {
        std::cout << "[CashGateClient::mCashLoginRequest][info]\n";
        std::cout << bodyData.Utf8DebugString();
    }
    mRequest(data, messageId);
}


void CashGateClient::mCashCreateOrderRequest (const std::string & requestId, const extsocks::NewOrder & newOrder) // 200
{
    int messageId {static_cast<int>(SocketMessageType::CASH_CREATE_ORDER_REQUEST)};
    int debugLevel = mSocketClientSetting.getDebugLevel();
    std::string data;
    extsocks::CashCreateOrderRequest bodyData;
    bodyData.set_requestid(requestId);
    bodyData.mutable_neworder()->CopyFrom(newOrder);
    bodyData.SerializeToString(&data);
    if (debugLevel >= 2) {
        std::cout << "[CashGateClient::mCashCreateOrderRequest][info]\n";
        std::cout << bodyData.Utf8DebugString();
    }
    mRequest(data, messageId);
}

void CashGateClient::mCashCloseOrderRequest (const std::string & requestId, const std::string & orderId,
                                             const double & amountToClose)
{
    int messageId {static_cast<int>(SocketMessageType::CASH_CLOSE_ORDER_REQUEST)};
    int debugLevel = mSocketClientSetting.getDebugLevel();
    std::string data;
    extsocks::CashCloseOrderRequest bodyData;
    bodyData.set_requestid(requestId);
    bodyData.set_orderid(orderId);
    bodyData.set_amounttoclose(amountToClose);
    bodyData.SerializeToString(&data);
    if (debugLevel >= 2) {
        std::cout << "[CashGateClient::mCashCloseOrderRequest][info]\n";
        std::cout << bodyData.Utf8DebugString();
    }
    mRequest(data, messageId);
}