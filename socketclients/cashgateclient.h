﻿#ifndef CASHGATECLIENTEXTSOCSNDN_202010_H
#define CASHGATECLIENTEXTSOCSNDN_202010_H

#include <sstream>
#include <mutex>
#include <thread>
#include <chrono>
#include <stdio.h>
#include <string>
#include <iostream>
#include <iomanip>
#include <random>
#include <ctime>

#include <extsocks-protocol.pb.h>
#include <socketclients/socketclient.h>

using namespace ru::smartwallet;


class CashGateClient : public SocketClient
{
public:
    CashGateClient() {}
    void testConnectionToCashGate();
    //void setMerchantId(const std::string & merchantId) {mMerchantId = merchantId;}
    //void setNewOrder(const extsocks::NewOrder & newOrder) {mNewOrder = newOrder;}
    //void setOrderToClose(const std::string & orderToClose){mOrderToClose = orderToClose;}
    //void setAmountToClose(double amountToClose){mAmountToClose = amountToClose;}
    bool createOrder(const extsocks::NewOrder & newOrder);
    bool closeOrder(const std::string & orderId, double amountToClose);
    //std::string getOrderId() const {std::unique_lock<std::mutex> lock(mOrderIdMutex); return mOrderId;};
    virtual ~CashGateClient(){}

private:
    //std::string mMerchantId {""};
    //std::string mOrderToClose {""};
    //double mAmountToClose {0.0};

    //extsocks::NewOrder mNewOrder;

    //mutable std::mutex mOrderIdMutex;
    //std::string mOrderId {""};

    // Request
    void mCashLoginRequest (std::string merchantId, std::string token, std::string storeId, std::string cashId,
                            std::string requestId); // 10

    void mCashCreateOrderRequest (const std::string & requestId, const extsocks::NewOrder & newOrder); // 200

    void mCashCloseOrderRequest (const std::string & requestId, const std::string & orderId,
                                 const double & amountToClose); // 230
};


#endif