﻿#include "stpgateclient.h"


StpGateClient::StpGateClient() {}


bool StpGateClient::payByFace(const std::string & orderId, const std::string & face)
{
    {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mStatus = SocketClientStatus::STP_PROCESSING_STARTED;
    }

    int debugLevel = mSocketClientSetting.getDebugLevel();
    std::string deviceId = mMerchantSetting.getDeviceId();

    try {
        mRequestId = std::to_string(mGen());
        int code = -1;
        std::string stpPaymentId{""};
        int availablePoints = 0;

        // request 10, response 11
        mLoginRequestMessage(mRequestId, mMerchantSetting.getMerchantId(), mMerchantSetting.getStoreId(),
                             mMerchantSetting.getCashId(), mMerchantSetting.getToken());
        extsocks::LoginResponseMessage loginResponseMessage = std::get<extsocks::LoginResponseMessage>(
            mResponse(SocketMessageType::LOGIN_RESPONSE_MESSAGE));

        // request 60, response 61
        mSendFrameRequestMessage(mRequestId, face, orderId, deviceId);
        extsocks::StpPaymentResponseMessage
        stpPaymentResponseMessageFirst = std::get<extsocks::StpPaymentResponseMessage>(
            mResponse(SocketMessageType::STP_PAYMENT_RESPONSE_MESSAGE));
        code = stpPaymentResponseMessageFirst.code();
        stpPaymentId = stpPaymentResponseMessageFirst.stppaymentid();

        if (code != 0 && code != 1 && code != 15) {
            std::stringstream sstream;
            sstream << "Error StpPaymentResponse code = " << code;
            throw std::runtime_error(sstream.str().c_str());
        }

        mAttemptPincode = 1;
        if (code == 1) {
            {
                std::unique_lock<std::mutex> lock(mStatusMutex);
                mStatus = SocketClientStatus::STP_WAIT_PINCODE;
            }
            if (debugLevel >= 2) {
                std::cout << "[StpGateClient::payByFace][info] pinCode attempt number:"
                          << "before getPincode (code==1), mAttemptPincode: " 
                          << mAttemptPincode << "\n";
            }
            std::string pin;
            while (pin.empty()) {
                if (mIsBreakPayment) {
                    std::string message = "[StpGateClient::payByFace][wait pin] break payment";
                    if (debugLevel >= 1) {
                        std::cout << message << std::endl;
                    }
                    throw std::runtime_error(message);
                }
                if (debugLevel >= 4) {
                    std::cout << "[StpGateClient::payByFace][debug] checking mAttemptPincode = " 
                              << mAttemptPincode << std::endl;
                }
                pin = mGetPinCode();
                std::this_thread::sleep_for(std::chrono::microseconds(1));
            }
            ++mAttemptPincode;
            mPincodeProcessRequestMessage(mRequestId, pin, stpPaymentId, deviceId);
            mClearPinCode();
            extsocks::StpPaymentResponseMessage
            stpPaymentResponseMessageCode1 = std::get<extsocks::StpPaymentResponseMessage>(
                mResponse(SocketMessageType::STP_PAYMENT_RESPONSE_MESSAGE));
            code = stpPaymentResponseMessageCode1.code();
            availablePoints = stpPaymentResponseMessageCode1.order().availablepoints();

            while (code == 13) {
                {
                    std::unique_lock<std::mutex> lock(mStatusMutex);
                    mStatus = SocketClientStatus::STP_INCORRECT_PINCODE;
                }
                if (debugLevel >= 2) {
                    std::cout << "[StpGateClient::payByFace][info] pinCode attempt number: " 
                              << "before getPincode (code==13), mAttemptPincode: " 
                              << mAttemptPincode << "\n";
                }
                std::string pin2;
                while (pin2.empty()) {
                    if (mIsBreakPayment) {
                        std::string message = "[StpGateClient::payByFace][wait pin2] break payment";
                        if (debugLevel >= 1) {
                            std::cout << message << std::endl;
                        }
                        throw std::runtime_error(message);
                    }
                    if (debugLevel >= 4) {
                        std::cout << "[StpGateClient::payByFace][more] checking pin: mAttemptPincode: " 
                                  << mAttemptPincode << "\n";
                    }
                    pin2 = mGetPinCode();
                    std::this_thread::sleep_for(std::chrono::microseconds(1));
                }
                mPincodeProcessRequestMessage(mRequestId, pin2, stpPaymentId, deviceId);
                mClearPinCode();
                pin2.clear();

                extsocks::StpPaymentResponseMessage
                stpPaymentResponseMessageCode13 = std::get<extsocks::StpPaymentResponseMessage>(
                    mResponse(SocketMessageType::STP_PAYMENT_RESPONSE_MESSAGE));
                code = stpPaymentResponseMessageCode13.code();
                availablePoints = stpPaymentResponseMessageCode13.order().availablepoints();
                ++mAttemptPincode;
            }
        }

        mAttemptPoints = 1;
        std::set<short> pointsAttempts;
        if (code == 15) {
            mAvailablePoints = 0;
            {
                std::unique_lock<std::mutex> lock(mStatusMutex);
                mStatus = SocketClientStatus::STP_WAIT_POINTS;
                mAvailablePoints = availablePoints;
            }
            int points = -1;
            while (points == -1) {
                if (debugLevel >= 4) {
                    std::cout << "[StpGateClient][more] checking points\n";
                }
                if (mIsBreakPayment) {
                    std::string message = "[StpGateClient::payByFace][wait points] break payment";
                    if (debugLevel >= 1) {
                        std::cout << message << std::endl;
                    }
                    throw std::runtime_error(message);
                }
                int tmp = mGetPointsForPayment();
                if (tmp == -1) continue;
                points = tmp;
                pointsAttempts.insert(points);
                if (points < 0 || points > availablePoints) {
                    std::unique_lock<std::mutex> lock(mStatusMutex);
                    mStatus = SocketClientStatus::STP_INCORRECT_POINTS;
                    points = -1;
                    std::this_thread::sleep_for(std::chrono::microseconds(100));
                    mAttemptPoints = pointsAttempts.size()+1;
                } else {
                    std::unique_lock<std::mutex> lock(mStatusMutex);
                    mStatus = SocketClientStatus::STP_WAIT_ORDER ;
                }
                std::cout << "+++ mAttemptPoints = " << mAttemptPoints << std::endl;
                if (mAttemptPoints>3) throw std::runtime_error("too many points attempt");
                std::this_thread::sleep_for(std::chrono::microseconds(10));
            }
            mClearPointsForPayment();
            mPayWithPointsRequestMessage(mRequestId, stpPaymentId, points, deviceId);
            extsocks::StpPaymentResponseMessage
            stpPaymentResponseMessageCode15 = std::get<extsocks::StpPaymentResponseMessage>(
                mResponse(SocketMessageType::STP_PAYMENT_RESPONSE_MESSAGE));
            code = stpPaymentResponseMessageCode15.code();
            availablePoints = stpPaymentResponseMessageCode15.order().availablepoints();
        }

        if (code == 16) {
            if (debugLevel >= 1) {
                std::cout << "[StpGateClient::payByFace] attempts to enter a pincode exceeded (code==16):"
                          << "mAttemptPincode = " 
                          << mAttemptPincode << std::endl;
            }
            throw std::runtime_error("pin attempt exceeded");
        }

        {
            std::unique_lock<std::mutex> lock(mStatusMutex);
            mStatus = SocketClientStatus::STP_WAIT_ORDER;
        }

        extsocks::OrderStatusUpdatedMessage
        orderStatusUpdatedMessage = std::get<extsocks::OrderStatusUpdatedMessage>(
            mResponse(SocketMessageType::ORDER_STATUS_UPDATE_MESSAGE));
        double paid = orderStatusUpdatedMessage.order().paid();
        double swipDiscount = orderStatusUpdatedMessage.order().swipdiscount();
        //int withdrawPoints = orderStatusUpdatedMessage.order().withdrawpoints();
        //double pointsRate  = orderStatusUpdatedMessage.order().pointsrate();
        //double pointsAsDiscount = orderStatusUpdatedMessage.order().pointsasdiscount();
        //double total = orderStatusUpdatedMessage.order().total();
        double totalOriginal = orderStatusUpdatedMessage.order().totaloriginal();
        std::string orderError = orderStatusUpdatedMessage.order().error();

        if (fabs(paid + swipDiscount - totalOriginal) < mEpsilon) {
            std::unique_lock<std::mutex> lock(mStatusMutex);
            mStatus = SocketClientStatus::STP_SUCCESSFULLY_PAID;
        } else throw std::runtime_error(orderError);

        {
            std::unique_lock<std::mutex> lock(mOrderMutex);
            mOrder = orderStatusUpdatedMessage.order();
        }

    } catch (const std::exception & exct) {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mErrorMessage = exct.what();
        mStatus = SocketClientStatus::SOCKET_CLIENT_FAILED;
        if (debugLevel >= 1) {
            std::cout << "[StpGateClient::payByFace][error] " << mErrorMessage << std::endl;
        }
        return false;
    } catch (...) {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mErrorMessage = "Unhandled pay_by_face exception";
        mStatus = SocketClientStatus::SOCKET_CLIENT_FAILED;
        if (debugLevel >= 1) {
            std::cout << "[StpGateClient::payByFace][error] catch unknow exception" << mErrorMessage << std::endl;
        }
        return false;
    }
    return true;
}


bool StpGateClient::IdentificationByFace(const std::string & requestId, const std::string & face)
{
    {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mStatus = SocketClientStatus::STP_PROCESSING_STARTED;
    }

    int debugLevel = mSocketClientSetting.getDebugLevel();
    std::string deviceId = mMerchantSetting.getDeviceId();

    try {
        mRequestId = std::to_string(mGen());
        int code = -1;
        std::string stpPaymentId{""};
        int availablePoints = 0;

        // request 10, response 11
        mLoginRequestMessage(mRequestId, mMerchantSetting.getMerchantId(), mMerchantSetting.getStoreId(),
                             mMerchantSetting.getCashId(), mMerchantSetting.getToken());
        extsocks::LoginResponseMessage loginResponseMessage = std::get<extsocks::LoginResponseMessage>(
            mResponse(SocketMessageType::LOGIN_RESPONSE_MESSAGE));

        // 130
        // message IdentificationRequest {
        // string requestId = 1;
        // string frame = 2;
        // }

        // 131
        // message IdentificationResponse {
        // string requestId = 1;
        // Customer customer = 2;
        // Error error = 3;
        // string attemptId = 4;
        // }

        // request 130, response 131
        mIdentificationRequest(mRequestId, face);
        extsocks::IdentificationResponse
        stpPaymentResponseMessageFirst = std::get<extsocks::IdentificationResponse>(
            mResponse(SocketMessageType::IDENTIFICATION_RESPONSE));

    } catch (const std::exception & exct) {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mErrorMessage = exct.what();
        mStatus = SocketClientStatus::SOCKET_CLIENT_FAILED;
        if (debugLevel >= 1) {
            std::cout << "[StpGateClient::payByFace][error] " << mErrorMessage << std::endl;
        }
        return false;
    } catch (...) {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mErrorMessage = "Unhandled pay_by_face exception";
        mStatus = SocketClientStatus::SOCKET_CLIENT_FAILED;
        if (debugLevel >= 1) {
            std::cout << "[StpGateClient::payByFace][error] catch unknow exception" << mErrorMessage << std::endl;
        }
        return false;
    }
    return true;
}





// --- Request Message ---

void StpGateClient::mSendFrameRequestMessage (const std::string & requestId, const std::string & warpFrame,
                                              const std::string & orderId, const std::string & deviceId) // 60
{
    int debugLevel = mSocketClientSetting.getDebugLevel();
    int messageId = static_cast<int>(SocketMessageType::SEND_FRAME_REQUEST_MESSAGE);
    std::string data;
    extsocks::SendFrameRequestMessage bodyData;
    bodyData.set_requestid(requestId);
    bodyData.set_rawframe(" ");
    bodyData.set_warpframe(warpFrame);
    bodyData.set_orderid(orderId);
    bodyData.set_deviceid(deviceId);
    bodyData.SerializeToString(&data);
    if (debugLevel >= 2) {
        std::cout << "[StpGateClient] (request " << messageId << ")" << std::endl;
        std::cout << "    requestId = " << requestId << std::endl;
        std::cout << "    warpFrame = ";
        for (int j=0; j<10; ++j) std::cout << warpFrame[j];
        std::cout << " ... " << std::endl;
        std::cout << "    orderId = " << orderId << std::endl;
        std::cout << "    deviceId = " << deviceId << std::endl;
    }
    mRequest(data, messageId);
}


void StpGateClient::mIdentificationRequest (const std::string & requestId, const std::string & warpFrame) // 130
{
    int debugLevel = mSocketClientSetting.getDebugLevel();
    int messageId = static_cast<int>(SocketMessageType::IDENTIFICATION_REQUEST);
    std::string data;
    extsocks::IdentificationRequest bodyData;
    bodyData.set_requestid(requestId);
    bodyData.set_frame(warpFrame);
    bodyData.SerializeToString(&data);
    if (debugLevel >= 2) {
        std::cout << "[StpGateClient] (request " << messageId << ")" << std::endl;
        std::cout << "    requestId = " << requestId << std::endl;
        std::cout << "    warpFrame = ";
        for (int j=0; j<10; ++j) std::cout << warpFrame[j];
        std::cout << " ... " << std::endl;
    }
    mRequest(data, messageId);
}

void StpGateClient::mPincodeProcessRequestMessage(const std::string & requestId, const std::string & pincode,
                                                  const std::string & stpPaymentId, const std::string & deviceId)// 30
{
    int debugLevel = mSocketClientSetting.getDebugLevel();
    int messageId = static_cast<int>(SocketMessageType::PINCODE_PROCESS_REQUEST_MESSAGE);
    std::string data;
    extsocks::PincodeProcessRequestMessage bodyData;
    bodyData.set_requestid(requestId);
    bodyData.set_pincode(pincode);
    bodyData.set_stppaymentid(stpPaymentId);
    bodyData.set_deviceid(deviceId);
    bodyData.SerializeToString(&data);
    if (debugLevel >= 2) {
        std::cout << "[StpGateClient::mPincodeProcessRequestMessage][info] (request "
                  << messageId << ")" << std::endl;
        bodyData.PrintDebugString();
    }
    mRequest(data, messageId);
}


void StpGateClient::mPayWithPointsRequestMessage(const std::string & requestId, const std::string & stpPaymentId,
                                                 int points, const std::string & deviceId) // 40
{
    int debugLevel = mSocketClientSetting.getDebugLevel();
    int messageId = static_cast<int>(SocketMessageType::PAY_WITH_POINTS_REQUEST_MESSAGE);
    std::string data;
    extsocks::PayWithPointsRequestMessage bodyData;
    bodyData.set_requestid(requestId);
    bodyData.set_stppaymentid(stpPaymentId);
    bodyData.set_withdrawpointsamount(points);
    bodyData.set_deviceid(deviceId);
    bodyData.SerializeToString(&data);
    if (debugLevel >= 2) {
        std::cout << "[StpGateClient::mPayWithPointsRequestMessage][info] (request "
                  << messageId << ")" << std::endl;
        bodyData.PrintDebugString();
    }
    mRequest(data, messageId);
}

