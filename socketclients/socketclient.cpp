#include "socketclient.h"


SocketClient::SocketClient() {
    mGen.seed(time(0));
}


void SocketClient::connect()
{
    int debugLevel = mSocketClientSetting.getDebugLevel();
    int sslSocketDebugLevel = mSocketClientSetting.getSslSocketDebugLevel();
    int sslSocketRcvTimeout = mSocketClientSetting.getSslSocketRcvTimeout();
    int sslSocketSndTimeout = mSocketClientSetting.getSslSocketSndTimeout();
    std::string ip = mSocketClientSetting.getIp();
    int port = mSocketClientSetting.getPort();
    mSslSocket = new SslSocket(ip.c_str(), port, sslSocketDebugLevel, sslSocketRcvTimeout, sslSocketSndTimeout);
    SslSocketStatus sslSocketStatus = mSslSocket->getStatus();
    if (sslSocketStatus == SslSocketStatus::SSL_CONNECTED) {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mStatus = SocketClientStatus::SOCKET_CLIENT_SSL_CONNECTED;
    } else {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mStatus = SocketClientStatus::SOCKET_CLIENT_SSL_NO_CONNECTION;
        mErrorMessage = mSslSocket->getErrorMessage();
        if (debugLevel >= 1) {
            std::cout << "[SocketClient::SocketClient][error] SssSocket failed: ";
            std::cout << mErrorMessage;
            std::cout << std::endl;
        }
    }
}


SocketClientStatus SocketClient::getStatus() const {
    std::unique_lock<std::mutex> lock(mStatusMutex);
    return mStatus;
}


std::string SocketClient::getErrorMessage() const {
    std::unique_lock<std::mutex> lock(mStatusMutex);
    return mErrorMessage;
}



void SocketClient::testConnectionToSTPGate() {
    {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mStatus = SocketClientStatus::STP_PROCESSING_STARTED;
    }
    std::string merchantId = mMerchantSetting.getMerchantId();
    std::string storeId = mMerchantSetting.getStoreId();
    std::string cashId = mMerchantSetting.getCashId();
    std::string token = mMerchantSetting.getToken();
    mRequestId = std::to_string(mGen());
    mLoginRequestMessage(mRequestId, merchantId, storeId, cashId, token);
    extsocks::LoginResponseMessage loginResponseMessage = std::get<extsocks::LoginResponseMessage>(
            mResponse(SocketMessageType::LOGIN_RESPONSE_MESSAGE));
    {
        std::unique_lock<std::mutex> lock(mStatusMutex);
        mStatus = SocketClientStatus::SOCKET_CLIENT_LOGGED_IN;
    }
}


void SocketClient::mRequest(const std::string & data, int messageId) {
    int debugLevel = mSocketClientSetting.getDebugLevel();
    char headerData[22] = {0};
    int bodySize = data.size();

    headerData[0] = (bodySize >> 24) & 0xFF;
    headerData[1] = (bodySize >> 16) & 0xFF;
    headerData[2] = (bodySize >> 8) & 0xFF;
    headerData[3] = bodySize & 0xFF;
    headerData[4] = (messageId >> 8) & 0xFF;
    headerData[5] = messageId & 0xFF;

    if (debugLevel >= 3) {
        std::cout << "[SocketClient::mRequest][debug] " << messageId << " {";
        for (int j = 0; j < 22; ++j)
            std::cout << std::setfill('0') << std::setw(2)
                      << std::hex << static_cast<int>((unsigned char)headerData[j]) << " ";

        int N = messageId == 60 ? 50 : bodySize;
        for (int j = 0; j < N; ++j)
            std::cout << std::setfill('0') << std::setw(2)
                      << std::hex << static_cast<int>((unsigned char)data[j]) << " ";
        std::cout << std::dec << "}" << std::endl;
    }

    mSslSocket->SendBuffer(headerData, 22);
    if (mSslSocket->getStatus() == SslSocketStatus::SSL_SENT) {
        mSslSocket->SendBuffer(data.c_str(), bodySize);
    }

    if (debugLevel >= 3) {
        std::cout << "[SocketClient::mRequest][debug] mSslSocket = " << mSslSocket->getStatus() << std::endl;
    }

    if (mSslSocket->getStatus() != SslSocketStatus::SSL_SENT) {
        std::string errorMessage = mSslSocket->getErrorMessage();
        if (debugLevel >= 1) {
            std::cout << "[SocketClient::mRequest][error] SssSocket failed: ";
            std::cout << errorMessage << std::endl;
        }
        throw std::runtime_error(errorMessage.c_str());
    }
}


AnySocketMessagesResponse SocketClient::mResponse(const SocketMessageType & socketMessageType) {
    int messageId;
    int bodySize;
    char* bodyArray;
    char headerData[22];
    int debugLevel = mSocketClientSetting.getDebugLevel();

    mSslSocket->RecvBuffer(headerData, 22);

    if (mSslSocket->getStatus() == SslSocketStatus::SSL_RECEIVIED) {

        if (debugLevel >= 3) {
            std::cout << "[SocketClient::mResponse][debug] the header was read: " ;
            for (int j = 0; j < 22; ++j)
                std::cout << std::setfill('0') << std::setw(2)
                          << std::hex << static_cast<int>((unsigned char) headerData[j]) << " ";
            std::cout << std::endl;
        }

        bodySize = int((unsigned char) (headerData[0]) << 24 |
                       (unsigned char) (headerData[1]) << 16 |
                       (unsigned char) (headerData[2]) << 8 |
                       (unsigned char) (headerData[3]));
        messageId = ((unsigned char) (headerData[4]) << 8 |
                     (unsigned char) (headerData[5]));
        bodyArray = new char[bodySize];
        if (debugLevel >= 3) {
            std::cout << std::dec << "[SocketClient::mResponse][debug] bodySize = " << bodySize
                      << " messageId = " << messageId << std::endl;
        }
        mSslSocket->RecvBuffer(bodyArray, bodySize);
    }

    if (debugLevel >= 3) {
        std::cout << "[SocketClient::mResponse][debug] mSslSocket = " << mSslSocket->getStatus() << std::endl;
    }

    if (mSslSocket->getStatus() == SslSocketStatus::SSL_FAILED) {
        if (debugLevel >= 1) {
            std::cout << "[SocketClient::mResponse][error] SssSocket failed: ";
            std::cout << mErrorMessage << std::endl;
        }
        throw std::runtime_error(mSslSocket->getErrorMessage());
    } else {
        if (debugLevel >= 3) {
            std::cout << "[SocketClient::mResponse][debug] SocketResponse " << bodySize << " " << messageId << " {";
            for (int j = 0; j < 22; ++j)
                std::cout << std::setfill('0') << std::setw(2)
                          << std::hex << static_cast<int>((unsigned char)headerData[j]) << " ";
            for (int j = 0; j < bodySize; ++j)
                std::cout << std::setfill('0') << std::setw(2)
                          << std::hex << static_cast<int>((unsigned char)bodyArray[j]) << " ";
            std::cout << std::dec << "}" << std::endl;
        }

        mSocketMessage = std::string(bodyArray, bodySize);
        mSocketMessageType = static_cast<SocketMessageType>(messageId);

        if (mSocketMessageType == SocketMessageType::ERROR_MESSAGE) {
            extsocks::ErrorMessage errorMessage;
            if (!errorMessage.ParseFromString(mSocketMessage))
                throw std::runtime_error("ParseFromString error for ParseErrorMessage");
            if (debugLevel >= 2) {
                std::cout << "[SocketClient::mResponse][info]\n";
                std::cout << errorMessage.Utf8DebugString();
            }
            std::stringstream errStream;
            errStream << "Received ErrorMessage with code = " << errorMessage.errorcode()
                      << " (" << errorMessage.description() << ")";
            throw std::runtime_error(errStream.str().c_str());
        }

        if (mSocketMessageType != socketMessageType) {
            std::stringstream sstream;
            sstream << "expected " << socketMessageType << " but came " << mSocketMessageType;
            throw std::runtime_error(sstream.str().c_str());
        }
    }

    switch (mSocketMessageType) {
        case SocketMessageType::LOGIN_RESPONSE_MESSAGE: {
            extsocks::LoginResponseMessage resp;
            resp.ParseFromString(mSocketMessage);
            if (debugLevel >= 2) {
                std::cout << "[SocketClient::mResponse][info] " << mSocketMessageType << std::endl;
                std::cout << resp.Utf8DebugString();
            }
            return resp;
        }
        case SocketMessageType::STP_PAYMENT_RESPONSE_MESSAGE: {
            extsocks::StpPaymentResponseMessage resp;
            resp.ParseFromString(mSocketMessage);
            if (debugLevel >= 2) {
                std::cout << "[SocketClient::mResponse][info] " << mSocketMessageType << std::endl;
                std::cout << resp.Utf8DebugString();
            }
            return resp;
        }
        case SocketMessageType::RAW_FRAME_RESPONSE_MESSAGE: {
            extsocks::RawFrameRequestMessage resp;
            resp.ParseFromString(mSocketMessage);
            if (debugLevel >= 2) {
                std::cout << "[SocketClient::mResponse][info] " << mSocketMessageType << std::endl;
                std::cout << resp.Utf8DebugString();
            }
            return resp;
        }
        case SocketMessageType::ORDER_STATUS_UPDATE_MESSAGE: {
            extsocks::OrderStatusUpdatedMessage resp;
            resp.ParseFromString(mSocketMessage);
            if (debugLevel >= 2) {
                std::cout << "[SocketClient::mResponse][info] " << mSocketMessageType << std::endl;
                std::cout << resp.Utf8DebugString();
            }
            return resp;

        }
        case SocketMessageType::CASH_CREATE_ORDER_RESPONSE: {
            extsocks::CashCreateOrderResponse resp;
            resp.ParseFromString(mSocketMessage);
            if (debugLevel >= 2) {
                std::cout << "[SocketClient::mResponse][info] " << mSocketMessageType << std::endl;
                std::cout << resp.Utf8DebugString();
            }
            return resp;
        }
        case SocketMessageType::CASH_CLOSE_ORDER_RESPONSE: {
            extsocks::CashCloseOrderResponse resp;
            resp.ParseFromString(mSocketMessage);
            if (debugLevel >= 2) {
                std::cout << "[SocketClient::mResponse][info] " << mSocketMessageType << std::endl;
                std::cout << resp.Utf8DebugString();
            }
            return resp;
        }
    }
}


// --- Request Message ---

void SocketClient::mLoginRequestMessage(const std::string & requestId, const std::string & userName,
                                        const std::string & storeId, const std::string & cashId,
                                        const std::string & token) // 10
{
    int debugLevel = mSocketClientSetting.getDebugLevel();
    int messageId = static_cast<int>(SocketMessageType::LOGIN_REQUEST_MESSAGE);
    std::string data;
    extsocks::LoginRequestMessage bodyData;
    bodyData.set_requestid(requestId);
    bodyData.set_username(userName);
    bodyData.set_storeid(storeId);
    bodyData.set_cashid(cashId);
    bodyData.set_token(token);
    bodyData.SerializeToString(&data);
    if (debugLevel >= 2) {
        std::cout << "[SocketClient::mLoginRequestMessage][info] (request " << messageId << ")" << std::endl;
        bodyData.PrintDebugString();
    }
    mRequest(data, messageId);
}

SocketClient::~SocketClient () {
    if (mSslSocket) {
        delete mSslSocket;
        mSslSocket = nullptr;
    }
}


// --- SocketClientStatus

std::ostream& operator<< (std::ostream &outStream, const SocketClientStatus & socketClientStatus) {
    switch (socketClientStatus)
    {
        // --- Common
        case SocketClientStatus::SOCKET_CLIENT_INIT:
            outStream << "SOCKET_CLIENT_INIT";
            break;
        case SocketClientStatus::SOCKET_CLIENT_SSL_CONNECTED:
            outStream << "SOCKET_CLIENT_SSL_CONNECTED";
            break;
        case SocketClientStatus::SOCKET_CLIENT_SSL_NO_CONNECTION:
            outStream << "SOCKET_CLIENT_SSL_NO_CONNECTION";
            break;
        case SocketClientStatus::SOCKET_CLIENT_FAILED:
            outStream << "SOCKET_CLIENT_FAILED";
            break;
        case SocketClientStatus::SOCKET_CLIENT_LOGGED_IN:
            outStream << "SOCKET_CLIENT_LOGGED_IN";
            break;

        // --- Cash Gate Client
        case SocketClientStatus::CASH_PROCESSING_STARTED:
            outStream << "CASH_PROCESSING_STARTED";
            break;
        case SocketClientStatus::CASH_LOGGED_IN:
            outStream << "CASH_LOGGED_IN";
            break;
        case SocketClientStatus::CASH_ORDER_CREATED:
            outStream << "CASH_ORDER_CREATED";
            break;
        case SocketClientStatus::CASH_ORDER_CLOSED:
            outStream << "CASH_ORDER_CLOSED";
            break;

            // --- STP Gate Client
        case SocketClientStatus::STP_PROCESSING_STARTED:
            outStream << "STP_PROCESSING_STARTED";
            break;
        //case SocketClientStatus::STP_LOGGED_IN:
        //    outStream << "STP_LOGGED_IN";
        //    break;
        case SocketClientStatus::STP_PAYMENT_MESSAGE_RECEIVED:
            outStream << "STP_PAYMENT_MESSAGE_RECEIVED";
            break;
        case SocketClientStatus::STP_WAIT_PINCODE:
            outStream << "STP_WAIT_PINCODE";
            break;
        case SocketClientStatus::STP_INCORRECT_PINCODE:
            outStream << "STP_INCORRECT_PINCODE";
            break;
        case SocketClientStatus::STP_WAIT_POINTS:
            outStream << "STP_WAIT_POINTS";
            break;
        case SocketClientStatus::STP_INCORRECT_POINTS:
            outStream << "STP_INCORRECT_POINTS";
            break;
        case SocketClientStatus::STP_WAIT_ORDER:
            outStream << "STP_WAIT_ORDER";
            break;
        case SocketClientStatus::STP_SUCCESSFULLY_PAID:
            outStream << "STP_SUCCESSFULLY_PAID";
            break;

    }
    return outStream;
}


std::ostream& operator<< (std::ostream &outStream, const SocketClientSetting & socketClientSetting) {
    outStream
        << "[SocketClentSetting] {"
        << "ip = " << socketClientSetting.getIp() << ", "
        << "port = " << socketClientSetting.getPort() << ", "
        << "debugLevel = " << socketClientSetting.getDebugLevel() << ", "
        << "sslSocketRcvTimeout = " << socketClientSetting.getSslSocketRcvTimeout() << ", "
        << "sslSocketSndTimeout = " << socketClientSetting.getSslSocketSndTimeout() << ", "
        << "sslSocketDebugLevel = " << socketClientSetting.getSslSocketDebugLevel()
        << "}";
    return outStream;
}


std::ostream& operator<< (std::ostream &outStream, const MerchantSetting & merchantSetting) {
    outStream
            << "[MerchantSetting] {"
            << "merchantId = " << merchantSetting.getMerchantId() << ", "
            << "storeId = " << merchantSetting.getStoreId() << ", "
            << "cashId = " << merchantSetting.getCashId() << ", "
            << "token = " << merchantSetting.getToken()
            << "}";
    return outStream;
}
