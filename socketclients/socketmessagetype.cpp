#include <iostream>
#include "socketmessagetype.h"

std::ostream& operator<< (std::ostream &outStream, const SocketMessageType &socketMessageType)
{
    switch (socketMessageType) {

        case SocketMessageType::SOCKET_TYPE_EMPTY:
            outStream << "SOCKET_TYPE_EMPTY";
            break;


            // --- COMMON ---
        case SocketMessageType::HEART_BEAT_REQUEST:
            outStream << "HEAR_BEAT_REQUEST";
            break;

        case SocketMessageType::HEART_BEAT_RESPONSE:
            outStream << "HEART_BEAT_RESPONSE";
            break;

        case SocketMessageType::ERROR_MESSAGE:
            outStream << "ERROR_MESSAGE";
            break;

        case SocketMessageType::LOGIN_REQUEST_MESSAGE:
            outStream << "LOGIN_REQUEST_MESSAGE";
            break;

        case SocketMessageType::LOGIN_RESPONSE_MESSAGE:
            outStream << "LOGIN_RESPONSE_MESSAGE";
            break;


        // --- STP ---

        case SocketMessageType::SET_RATING_REQUEST_MESSAGE:
            outStream << "SET_RATING_REQUEST_MESSAGE";
            break;

        case SocketMessageType::SET_RATING_RESPONSE_MESSAGE:
            outStream << " SET_RATING_RESPONSE_MESSAGE";
            break;

        case SocketMessageType::PINCODE_PROCESS_REQUEST_MESSAGE:
            outStream << "PINCODE_PROCESS_REQUEST_MESSAGE";
            break;

        case SocketMessageType::PAY_WITH_POINTS_REQUEST_MESSAGE:
            outStream << "PAY_WITH_POINTS_REQUEST_MESSAGE";
            break;

        case SocketMessageType::SEND_WARP_FRAME_REQUEST_MESSAGE:
            outStream << "SEND_WARP_FRAME_REQUEST_MESSAGE";
            break;

        case SocketMessageType::SEND_FRAME_REQUEST_MESSAGE:
            outStream << "SEND_FRAME_REQUEST_MESSAGE";
            break;

        case SocketMessageType::STP_PAYMENT_RESPONSE_MESSAGE:
            outStream << "STP_PAYMENT_RESPONSE_MESSAGE";
            break;

        case SocketMessageType::RAW_FRAME_REQUEST_MESSAGE:
            outStream << "RAW_FRAME_REQUEST_MESSAGE";
            break;

        case SocketMessageType::RAW_FRAME_RESPONSE_MESSAGE:
            outStream << "RAW_FRAME_RESPONSE_MESSAGE";
            break;

        case SocketMessageType::CONFIG_REQUEST_MESSAGE:
            outStream << "CONFIG_REQUEST_MESSAGE";
            break;

        case SocketMessageType::CONFIG_RESPONSE_MESSAGE:
            outStream << "CONFIG_RESPONSE_MESSAGE";
            break;

        case SocketMessageType::ACTIVATE_DEVICE_REQUEST_MESSAGE:
            outStream << "ACTIVATE_DEVICE_REQUEST_MESSAGE";
            break;

        case SocketMessageType::ORDER_STATUS_UPDATE_MESSAGE:
            outStream << "ORDER_STATUS_UPDATE_MESSAGE";
            break;

        case SocketMessageType::IR_IMAGE_REQUEST_MESSAGE:
            outStream << "IR_IMAGE_REQUEST_MESSAGE";
            break;

        case SocketMessageType::IR_IMAGE_RESPONSE_MESSAGE:
            outStream << "IR_IMAGE_RESPONSE_MESSAGE";
            break;

        case SocketMessageType::DEVICE_LOG_MESSAGE:
            outStream << "DEVICE_LOG_MESSAGE";
            break;

        case SocketMessageType::UPDATE_AVAILABLE_MESSAGE:
            outStream << "UPDATE_AVAILABLE_MESSAGE";
            break;

        case SocketMessageType::ROLL_BACK_MESSAGE:
            outStream << "ROLL_BACK_MESSAGE";
            break;

        case SocketMessageType::IDENTIFICATION_REQUEST:
            outStream << "IDENTIFICATION_REQUEST";
            break;

        case SocketMessageType::IDENTIFICATION_RESPONSE:
            outStream << "IDENTIFICATION_RESPONSE";
            break;

        case SocketMessageType::IDENTIFICATION_PINCODE_REQUEST:
            outStream << "IDENTIFICATION_PINCODE_REQUEST";
            break;

        // --- CASH ---
        case SocketMessageType::CASH_LOGIN_REQUEST:
            outStream << "CASH_LOGIN_REQUEST: ";
            break;

        case SocketMessageType::CASH_CREATE_ORDER_REQUEST :
            outStream << "CASH_CREATE_ORDER_REQUEST";
            break;

        case SocketMessageType::CASH_CREATE_ORDER_RESPONSE:
            outStream << "CASH_CREATE_ORDER_RESPONSE";
            break;

        case SocketMessageType::CASH_GET_ORDER_REQUEST:
            outStream << "CASH_GET_ORDER_REQUEST";
            break;

        case SocketMessageType::CASH_GET_ORDER_RESPONSE:
            outStream << "CASH_GET_ORDER_RESPONSE";
            break;

        case SocketMessageType::CASH_GET_ORDER_LIST_REQUEST:
            outStream << "CASH_GET_ORDER_LIST_REQUEST";
            break;

        case SocketMessageType::CASH_GET_ORDER_LIST_RESPONSE:
            outStream << "CASH_GET_ORDER_LIST_RESPONSE";
            break;

        case SocketMessageType::CASH_CLOSE_ORDER_REQUEST:
            outStream << "CASH_CLOSE_ORDER_REQUEST";
            break;

        case SocketMessageType::CASH_CLOSE_ORDER_RESPONSE:
            outStream << "CASH_CLOSE_ORDER_RESPONSE";
            break;

        case SocketMessageType::CASH_ORDER_STATUS_UPDATED:
            outStream << "CASH_ORDER_STATUS_UPDATED";
            break;

        case SocketMessageType::CASH_SAVE_SETTING_REQUEST:
            outStream << " CASH_SAVE_SETTING_REQUEST";
            break;

        case SocketMessageType::CASH_SAVE_SETTING_RESPONSE:
            outStream << "CASH_SAVE_SETTING_RESPONSE";
            break;

        case SocketMessageType::CASH_GET_SETTING_REQUEST:
            outStream << "CASH_GET_SETTING_REQUEST";
            break;

        case SocketMessageType::CASH_GET_SETTING_RESPONSE:
            outStream << "CASH_GET_SETTING_RESPONSE";
            break;

        case SocketMessageType::CASH_ORDER_APPLY_MERCHANT_DISCOUNT_REQUEST:
            outStream << "CASH_ORDER_APPLY_MERCHANT_DISCOUNT_REQUEST";
            break;

        case SocketMessageType::CASH_ORDER_APPLY_MERCHANT_DISCOUNT_RESPONSE:
            outStream << "CASH_ORDER_APPLY_MERCHANT_DISCOUNT_RESPONSE";
            break;

        default: outStream << "UNKNOW_MESSAGE";
    }
    outStream << "(" << socketMessageType << ")";
    return outStream;
}
